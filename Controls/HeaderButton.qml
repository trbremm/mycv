import QtQuick 2.0
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.0

ToolButton {
    id: button
    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
    background: Rectangle {
        radius: 4
        anchors.fill: parent
        color: button.enabled && (button.checked || button.down) ? "#7EB18D" : "#00000000"
        opacity: button.down ? 0.5 : 1.0
        border.color: button.enabled && button.hovered ? "#7EB18D" : "#00000000"
        border.width: 2
    }
    icon.color: "white"
    checkable: true
    property alias tip_text: id_tooltip.text
    ToolTip {
        id: id_tooltip
        visible: button.hovered
        delay: 100
        timeout: 2000
        contentItem: Text{
            color: "white"
            text: id_tooltip.text
            font.bold: true
        }
        background: Rectangle {
            radius: 2
            color: "#AA7EB18D"
        }
    }
}
