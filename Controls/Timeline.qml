import QtQuick 2.0
import QtQuick.Controls 2.0
import WorkExperiences 1.0
import QtGraphicalEffects 1.0

Rectangle {
    id: timeline_root
    color: "#43464B"
    LinearGradient {
            anchors.fill: parent
            start: Qt.point(0, 0)
            end: Qt.point(timeline_root.width, 0)
            gradient: Gradient {
                GradientStop { position: 0.0; color: "#43464B" }
                GradientStop { position: 1.0; color: "#696B6F" }
            }
        }

    Repeater {
        id: years_list
        anchors.fill: parent
        model: parent.getTotalYears();

        Component {
            id: years_delegate
            Rectangle {
                x: 0
                y: workExperienceList ? getProportionToTime(Math.floor(workExperienceList.career_last_year) - index) : 0
                width: timeline_root.width
                height: 1.5
                border.color: "#10000000"
                Text {
                    id: year_text
                    font.family: "Courier New"
                    font.pointSize: 8
                    font.bold: true
                    color: "white"
                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    anchors.leftMargin: 4
                    text: workExperienceList ? "" + Math.floor(workExperienceList.career_last_year - index) : ""
                }
            }
        }
    }
    function getTotalYears(){
        return workExperienceList ? Math.ceil(workExperienceList.career_duration) : 0.0;
    }
    function getFormattedYear(value){
        let year = Math.floor(value);
        let month = Math.round((value - year) * 12 + 1);
        return year + "." + month;
    }
    function getProportionToTime(value) {
       if (workExperienceList) {
           return (workExperienceList.career_last_year - value) / workExperienceList.career_duration * timeline_root.height;
       } else {
           return 0.0;
       }
    }
    function getProportionalDuration(value) {
       if (workExperienceList) {
           return period_list.height * value / workExperienceList.career_duration;
       } else {
           return 0.0;
       }
    }
    function getPriorityOffset(priority, width) {
        return timeline_root.width - ((priority + 1) * width * 2.0 / 3.0 + 8)
    }

    Repeater {
        id: period_list
        anchors.fill: parent
        model: WorkExperienceModel {
                    list: workExperienceList
               }

        Component {
            id: period_delegate
            Rectangle {
                id: period_rectangle
                implicitWidth: 20
                radius: 4
                // Height proportional to experience duration in relation to career
                height: getProportionalDuration(model.duration)
                // Y is defined by the proportional difference from latest career time to the end of the experience
                y: getProportionToTime(model.final_year)
                // Adds padding in case of parallel time periods.
                x: getPriorityOffset(model.priority, implicitWidth)
                color: mouse_area.containsMouse ? "#aa7eb18d" : "#7e8db1"
                border.color: mouse_area.containsMouse ? "#7eb18d" : "#aaffffff"
                border.width: 1
                ToolTip {
                    id: toolTip
                    visible: mouse_area.containsMouse
                    delay: 500
                    timeout: 2000
                    contentItem: Text{
                        color: "white"
                        text: timeline_root.getFormattedYear(model.starting_year) + "/" + timeline_root.getFormattedYear(model.final_year)
                        font.bold: true
                        font.family: "Courier New"
                    }
                    background: Rectangle {
                        color: "#f07e8db1"
                        radius: 4

                    }

                }
                MouseArea {
                    id: mouse_area
                    anchors.fill: parent
                    hoverEnabled: true
                    onPressed: {
                        workExperienceList.selected = index
                    }
                }
            }
        }
    }

    Connections {
            target: timeline_root
            function onWidthChanged(value) {
                timeline_root.updateSelector()
            }
            function onHeightChanged(value) {
                timeline_root.updateSelector()
            }
    }
    Connections {
            target: workExperienceList
            function onSelectedChanged(index) {
                timeline_root.updateSelector()
            }
    }
    Component.onCompleted: {
        timeline_root.updateSelector();
    }

    function updateSelector() {
        if (!workExperienceList) {
            return;
        }

        selector.height = getProportionalDuration(workExperienceList.getSelectedDurationInYears());
        selector.y = getProportionToTime(workExperienceList.getSelectedEndYear());
        selector.x = getPriorityOffset(workExperienceList.getSelectedPriority(), selector.width);
    }

    Rectangle {
        id: selector
        radius: 4
        border.color: "white"
        border.width: 1
        color:  "#7eb18d"
        height: 0
        width: 20
    }
}
