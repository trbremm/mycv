QT += quick

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        Model/cv_reader.cpp \
        Model/work_experience.cpp \
        Model/work_experience_model.cpp \
        main.cpp \
        Model/work_experience_list.cpp

RESOURCES += qml.qrc \
    data.qrc \
    imgs.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    cv_data.json

HEADERS += \
    Model/cv_reader.h \
    Model/work_experience.h \
    Model/work_experience_list.h \
    Model/work_experience_model.h
