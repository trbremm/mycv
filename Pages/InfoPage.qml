import QtQuick 2.0

Rectangle {
    id: root_rectangle
    anchors.fill: parent
    color: "#696b6f"
    border.color: "#696b6f"

    Text {
        id: name
        color: "#ffffff"
        text: qsTr("THIAGO BREMM")
        anchors.left: parent.left
        anchors.top: parent.top
        font.pixelSize: 26
        font.family: "Helvetica"
        anchors.topMargin: 50
        anchors.leftMargin: parent.width / 8
    }

    Text {
        id: nationality
        anchors.top: name.bottom
        anchors.left: name.left
        anchors.topMargin: 30
        anchors.leftMargin: 10
        color: "#ffffff"
        text: qsTr("Brazilian")
        font.pixelSize: 13
        font.family: "Helvetica"
    }

    Text {
        id: address
        width: 215
        height: 15
        anchors.top: nationality.bottom
        anchors.left: nationality.left
        anchors.topMargin: 10
        color: "#ffffff"
        text: qsTr("Aalsterweg 130A, Eindhoven - Noord Brabant, Netherlands - 5615 CJ")
        font.pixelSize: 13
        font.family: "Helvetica"
        wrapMode: Text.WordWrap
    }

    TextEdit {
        id: phone
        readOnly: true
        selectByMouse: true
        selectionColor: "#bfb6b1"
        anchors.top: email.bottom
        anchors.right: email.right
        anchors.topMargin: 15
        color: "#ffffff"
        text: qsTr("+310658973560")
        font.pixelSize: 14
        font.family: "Courier New"
        font.bold: true
        font.italic: true
    }

    TextEdit {
        id: email
        readOnly: true
        selectByMouse: true
        selectionColor: "#bfb6b1"
        anchors.bottom: nationality.bottom
        anchors.right: parent.right
        anchors.rightMargin: name.anchors.leftMargin
        color: "#ffffff"
        text: qsTr("trbremm@gmail.com")
        font.pixelSize: 14
        font.family: "Courier New"
        font.bold: true
        font.italic: true
    }

    Text {
        id: about
        anchors.top: address.bottom
        anchors.topMargin: 60
        anchors.left: name.left
        anchors.right: email.right
        color: "#ffffff"
        text: qsTr("I am a computer scientist who is focused on problem solving, assessing and evaluating the project needs and getting to the core of the issue to find the best and minimal solution possible. I strongly believe that teamwork, knowledge sharing and communication are the best way to accomplish difficult tasks and achieve goals. In that, the very code must communicate well. And most of all, user problems and needs must be understood so that actions are as effective as possible.")
        font.pixelSize: 14
        horizontalAlignment: Text.AlignJustify
        font.family: "Helvetica"
        wrapMode: Text.WordWrap
    }

}




