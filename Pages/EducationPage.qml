import QtQuick 2.0
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.12

Rectangle {
    id: root_rectangle
    anchors.fill: parent
    color: "#696B6F"
    border.color: "#696B6F"

    ColumnLayout {
        height: parent.height / 1.5
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: parent.width / 8
        anchors.rightMargin: parent.width / 8
        anchors.topMargin: 100

        Rectangle {
            Layout.fillWidth: true
            Layout.fillHeight: true
            color: "#00000000"
            Text {
                id: cco_degree
                color: "white"
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.topMargin: 10
                anchors.leftMargin: 10
                text: "• BSc in Computer Science"
                font.pixelSize: 16
                font.family: "Helvetica"
            }

            Text {
                id: cco_date
                color: "white"
                text: "Completed between 2008 - 2012"
                font.pixelSize: 14
                font.family: "Helvetica"
                anchors.top: cco_degree.top
                anchors.right: parent.right
                anchors.rightMargin: 10
            }

            Text {
                id: cco_uni
                color: "white"
                anchors.top: cco_degree.bottom
                anchors.right: cco_date.right
                anchors.topMargin: 10
                anchors.leftMargin: 10
                text: "at Federal University of Santa Catarina - UFSC"
                font.pixelSize: 14
                font.family: "Helvetica"
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.fillHeight: true
            color: "#00000000"
            Text {
                id: phys_degree
                color: "white"
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.topMargin: 10
                anchors.leftMargin: 10
                text: "• Incomplete BSc in Physics"
                font.pixelSize: 16
                font.family: "Helvetica"
            }

            Text {
                id: phys_date
                color: "white"
                text: "1 semester in 2007"
                font.pixelSize: 14
                font.family: "Helvetica"
                anchors.top: phys_degree.top
                anchors.right: parent.right
                anchors.rightMargin: 10
            }

            Text {
                id: phys_uni
                color: "white"
                anchors.top: phys_degree.bottom
                anchors.right: phys_date.right
                anchors.topMargin: 10
                anchors.leftMargin: 10
                text: "at Federal University of Santa Catarina - UFSC"
                font.pixelSize: 14
                font.family: "Helvetica"
            }
        }
    }
}
