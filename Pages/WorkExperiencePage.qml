import QtQuick 2.0
import QtQuick.Controls 2.0

import "../Controls"
import "../Views"

Rectangle {
    color: "pink"
    Timeline {
        id: timeline
        height: parent.height
        width: 90
    }

    WorkExperienceContent {
        id: content
        height: 300
        anchors.left: timeline.right
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
    }
}
