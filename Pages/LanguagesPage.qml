import QtQuick 2.0

Rectangle {
    id: root_rectangle
    anchors.fill: parent
    color: "#696B6F"
    border.color: "#696B6F"

    Column {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        spacing: 20
        Repeater {
            model: ["Portuguese (native)", "English (fluent)", "Spanish (intermediate)", "Dutch (basic)"]

            Component {
                id: delegate
                Text {
                    color: "white"
                    text: modelData
                    font.pixelSize: 16
                    font.family: "Helvetica"
                }
            }
        }
    }
}
