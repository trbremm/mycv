import QtQuick 2.0

Rectangle {
    id: root_rectangle
    anchors.fill: parent
    color: "#696B6F"
    border.color: "#696B6F"

    Column {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        spacing: 20
        Repeater {
            model: ["Improv Comedy actor and instructor", "Drawing", "Wood carving", "Former Kung Fu practicioner and instructor", "Cycling"]

            Component {
                id: delegate
                Text {
                    color: "white"
                    text: modelData
                    font.pixelSize: 16
                    font.family: "Helvetica"
                }
            }
        }
    }
}
