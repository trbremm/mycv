import QtQuick 2.0
import QtQuick.Controls 2.15

Item {

    Rectangle {
        id: root_rectangle
        anchors.fill: parent
        color: "#696B6F"
        border.color: "#696B6F"
        Connections {
                target: workExperienceList
                function onSelectedChanged(index) {
                    root_rectangle.updateInformation()

                }
        }
        Component.onCompleted: {
            updateInformation();
        }
        function updateInformation(){
            start_date.text = workExperienceList.getSelectedStartDate()
            end_date.text = workExperienceList.getSelectedEndDate()
            title.text = workExperienceList.getSelectedTitle()
            company.text = workExperienceList.getSelectedCompany()
            location.text = workExperienceList.getSelectedLocation()
            description.text = workExperienceList.getSelectedDescription()
        }

        Text {
            id: start_date
            font.family: "Courier New"
            font.pointSize: 13
            font.bold: true
            color: "#f5f6f5"
            text: "2015"
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.leftMargin: 30
            anchors.topMargin: 20
        }

        Text {
            id: date_separator
            font.family: "Courier New"
            font.pointSize: 13
            font.bold: true
            color: "#f5f6f5"
            text: "-"
            anchors.left: start_date.right
            anchors.verticalCenter: start_date.verticalCenter
            anchors.leftMargin: 10
        }

        Text {
            id: end_date
            font.family: "Courier New"
            font.pointSize: 13
            font.bold: true
            color: "#f5f6f5"
            text: "2018"
            anchors.leftMargin: 10
            anchors.verticalCenter: start_date.verticalCenter
            anchors.left: date_separator.right
        }


        Text {
            id: title
            anchors.top: start_date.bottom
            anchors.left: start_date.left
            anchors.topMargin: 25
            anchors.leftMargin: 5
            font.family: "Helvetica"
            font.pointSize: 14
            color: "white"
            text: ""
        }

        Text {
            id: company
            anchors.top: title.top
            anchors.right: parent.right
            anchors.rightMargin: 30
            anchors.topMargin: 0
            font.family: "Helvetica"
            font.pointSize: 14
            color: "white"
            text: "Company"
        }

        Text {
            id: location
            font.family: "Times New Roman"
            font.pointSize: 10
            font.bold: true
            font.italic: true
            color: "white"
            anchors.top: company.bottom
            anchors.right: company.right
            anchors.topMargin: 5
            text: ""
        }
        ScrollView {
            id: scrollview
            anchors.top: location.bottom
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 30
            anchors.topMargin: 20
            anchors.leftMargin: 20
            anchors.rightMargin: 20
            anchors.left: title.left
            anchors.right: company.right
            clip: true
            ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
            ScrollBar.vertical.policy: ScrollBar.AsNeeded
            Text {
                width: scrollview.width
                id: description
                font.family: "Helvetica"
                font.pointSize: 10
                color: "white"
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignJustify
                text: ""
            }
        }
    }
}
