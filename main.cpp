#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "Model/cv_reader.h"
#include "Model/work_experience.h"
#include "Model/work_experience_list.h"
#include "Model/work_experience_model.h"

#include <QDate>

void CreateDummyItems(WorkExperienceList& list) {
    list.addExperience({QDate(2019, 3, 15), QDate(2021, 1, 1), 0, QString(""), QString("blo"),QString(""),QString(""),{}, true});
    list.addExperience({QDate(2017, 10, 1), QDate(2018, 9, 1), 1, QString(""), QString("bli"),QString(""),QString(""),{}, true});
    list.addExperience({QDate(2017, 2, 1), QDate(2019, 1, 1), 0, QString(""), QString("ble"),QString(""),QString(""),{}, true});
    list.addExperience({QDate(2015, 1, 1), QDate(2015, 6, 1), 0, QString(""), QString("bla"),QString(""),QString(""),{}, true});
}

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    // This was necessary to avoid flickering during window resize in my machine (Windows 10 + NVidia GTX 1650)
    QCoreApplication::setAttribute(Qt::AA_UseOpenGLES);
    // -------------------------------------------------------------------------------------------------------

    qmlRegisterType<WorkExperienceModel>("WorkExperiences", 1, 0, "WorkExperienceModel");
    qmlRegisterUncreatableType<WorkExperienceList>("WorkExperiences", 1, 0, "WorkExperienceList",
       QStringLiteral("WorkExperienceList should not be created in QML"));

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    WorkExperienceList experience_list;
    // CreateDummyItems(experience_list);
    if (!CvReader::ParseFile(":/data/cv_data.json", experience_list)) {
        return 1;
    }
    // Make work experiences list globally available in QML context.
    engine.rootContext()->setContextProperty(QStringLiteral("workExperienceList"), &experience_list);

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
