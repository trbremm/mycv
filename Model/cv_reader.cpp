#include "cv_reader.h"

#include <QDate>
#include <QDebug>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

bool CvReader::ParseFile(const QString &filename, WorkExperienceList& list)
{
    QFile cv_file(filename);
    if (!cv_file.open(QIODevice::ReadOnly)) {
            qWarning() << "Couldn't open CV file: " + filename;
            return false;
    }

    QByteArray data = cv_file.readAll();
    QJsonParseError error;
    QJsonDocument const document = QJsonDocument::fromJson(data, &error);
    QJsonObject const root = document.object();
    int count = 0;
    // Parse work experiences
    if (root.contains("work_experiences") && root["work_experiences"].isArray()) {
        QJsonArray experiences_obj = root["work_experiences"].toArray();
        for(int i = 0; i < experiences_obj.size(); ++i) {
            auto const& element = experiences_obj[i];
            WorkExperience experience;
            if (ParseWorkExperience(element.toObject(), experience)) {
                list.addExperience(experience);
                ++count;
            } else {
                qWarning() << "Invalid experience found.";
            }
        }
    } else {
        qWarning() <<  "Error loading: " + filename + "; No 'work_experiences' list found.";
        return false;
    }

    qInfo() <<  "Parsed " << count << " work experiences. Total career time: " << list.getCareerDuration();
    return true;
}

bool CvReader::ParseWorkExperience(QJsonObject const element, WorkExperience& experience) {
    char const *keys[] = { "title", "company", "location", "description", "start_date", "end_date" };
    for (auto const key : keys) {
         if (!element.contains(key)) {
             return false;
         }
    }

    experience.title = element["title"].toString();
    experience.company = element["company"].toString();
    experience.location = element["location"].toString();
    experience.description = element["description"].toString();
    QString const date_format{"yyyy-MM-dd"};
    experience.start_date = QDate::fromString(element["start_date"].toString(), date_format);
    if (!experience.start_date.isValid()) {
        qWarning() <<  "Invalid start date: " + element["start_date"].toString() << "; Format should be: " << date_format;
        return false;
    }
    experience.end_date = QDate::fromString(element["end_date"].toString(), date_format);
    if (!experience.end_date.isValid()) {
        // Replace "current" by current Date object
        if (element["end_date"].toString() == "current") {
            experience.end_date = QDate::currentDate();
        } else {
            qWarning() <<  "Invalid start date: " + element["start_date"].toString();
            return false;
        }

    }

    experience.priority = 0;

    return true;

}
