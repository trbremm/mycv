#pragma once

#include <QDate>
#include <QObject>
#include <QSet>

namespace  {
float YearAsDouble(QDate const& date) {
    return date.year() + (float) date.dayOfYear() / date.daysInYear();
}
}

struct WorkExperience {
    QDate start_date;
    QDate end_date;
    int priority;
    QString title;
    QString company;
    QString location;
    QString description;
    QSet<QString> tech;
    bool highlighted;

    float StartingYear() const {
        return YearAsDouble(start_date);
    }

    float FinalYear() const {
        return YearAsDouble(end_date);
    }

    float DurationInYears() const {
        return FinalYear() - StartingYear();
    }
};
