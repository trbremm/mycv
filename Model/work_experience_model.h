#pragma once

#include <QAbstractListModel>

class WorkExperienceList;

class WorkExperienceModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(WorkExperienceList* list READ list WRITE setList)
public:
    explicit WorkExperienceModel(QObject *parent = nullptr);

    enum {
        PriorityRole = Qt::UserRole,
        DurationRole,
        StartingYearRole,
        FinalYearRole,
    };

    int rowCount(QModelIndex const& parent = QModelIndex()) const override;

    QVariant data(QModelIndex const& index, int role = Qt::DisplayRole) const override;

    virtual QHash<int, QByteArray> roleNames() const override;

    WorkExperienceList* list();
    void setList(WorkExperienceList *newList);

private:
    QHash<int, QByteArray> _role_names;
    WorkExperienceList* _list;

};
