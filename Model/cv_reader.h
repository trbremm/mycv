#pragma once

#include <QString>

#include "work_experience_list.h"


class CvReader
{
public:
    static bool ParseFile(QString const& filename, WorkExperienceList& list);
    static bool ParseWorkExperience(QJsonObject const json, WorkExperience& experience);

};
