#include "work_experience_list.h"

#include <algorithm>
#include <limits>
#include <QDebug>

WorkExperienceList::WorkExperienceList(QObject *parent) : QObject(parent), _earliest_year(std::numeric_limits<float>::max()), _latest_year(std::numeric_limits<float>::min()), _selected{-1}
{

}

const QVector<WorkExperience> &WorkExperienceList::items() const {
    return _items;
}

void WorkExperienceList::addExperience(WorkExperience experience) {
    _earliest_year = std::min(experience.StartingYear(), _earliest_year);
    _latest_year = std::max(experience.FinalYear(), _latest_year);

    // Define priority in case of parallel periods
    for (int i = 0; i < _items.size(); ++i) {
        auto& current = _items[i];
        bool intersect = (experience.end_date <= current.end_date && experience.end_date >= current.start_date) ||
                         (experience.start_date >= current.start_date && experience.start_date <= current.end_date);
        if ( intersect ) {
            // Smaller priority numbers mean the experience is more important,
            // choosing longest experience to be the case
            if (experience.DurationInYears() > current.DurationInYears()) {
                experience.priority = current.priority;
                ++current.priority;
            } else {
                ++experience.priority;
            }
        }
    }

    _items.append(experience);
    // Select latest by default
    if (_latest_year == experience.FinalYear()) {
        setSelected(_items.size() - 1);
    }

    emit careerChanged();
}

float WorkExperienceList::getCareerDuration() const
{
    return _latest_year - _earliest_year;
}

float WorkExperienceList::getCareerLastYear() const
{
    return _latest_year;
}

float WorkExperienceList::getCareerFirstYear() const
{
    return _earliest_year;
}

int WorkExperienceList::selected() const
{
    return _selected;
}

void WorkExperienceList::setSelected(int const index) {
    bool new_selection = index != _selected;
    _selected = index;
    if (new_selection) {
        emit selectedChanged(index);
    }
}

WorkExperience WorkExperienceList::getSelectedExperience() const
{
    if (_selected >= 0 && _selected < _items.size()) {
        return _items[_selected];
    }
    return {};
}

float WorkExperienceList::getSelectedDurationInYears() const
{
    return getSelectedExperience().DurationInYears();
}

float WorkExperienceList::getSelectedEndYear() const
{
    return getSelectedExperience().FinalYear();
}

int WorkExperienceList::getSelectedPriority() const
{
    return getSelectedExperience().priority;
}

QString WorkExperienceList::getSelectedTitle() const {
    return getSelectedExperience().title;
}

QString WorkExperienceList::getSelectedStartDate() const
{
    return getSelectedExperience().start_date.toString("yyyy.MM");
}

QString WorkExperienceList::getSelectedEndDate() const
{
    return getSelectedExperience().end_date.toString("yyyy.MM");
}

QString WorkExperienceList::getSelectedCompany() const
{
    return getSelectedExperience().company;
}

QString WorkExperienceList::getSelectedLocation() const
{
    return getSelectedExperience().location;
}

QString WorkExperienceList::getSelectedDescription() const
{
    return getSelectedExperience().description;
}
