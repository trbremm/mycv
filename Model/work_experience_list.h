#pragma once

#include <QDate>
#include <QObject>
#include <QSet>
#include <QVector>

#include "work_experience.h"

class WorkExperienceList : public QObject
{
    Q_OBJECT
    Q_PROPERTY(float career_duration READ getCareerDuration NOTIFY careerChanged)
    Q_PROPERTY(float career_last_year READ getCareerLastYear NOTIFY careerChanged)
    Q_PROPERTY(float career_first_year READ getCareerFirstYear NOTIFY careerChanged)
    Q_PROPERTY(int selected READ selected WRITE setSelected NOTIFY selectedChanged)
public:
    explicit WorkExperienceList(QObject *parent = nullptr);

    QVector<WorkExperience> const& items() const;

    void addExperience(WorkExperience experience);

    float getCareerDuration() const;
    float getCareerLastYear() const;
    float getCareerFirstYear() const;

    int selected() const;
    void setSelected(int const index);
    WorkExperience getSelectedExperience() const;

    Q_INVOKABLE float getSelectedDurationInYears() const;
    Q_INVOKABLE float getSelectedEndYear() const;
    Q_INVOKABLE int getSelectedPriority() const;
    Q_INVOKABLE QString getSelectedTitle() const;
    Q_INVOKABLE QString getSelectedStartDate() const;
    Q_INVOKABLE QString getSelectedEndDate() const;
    Q_INVOKABLE QString getSelectedCompany() const;
    Q_INVOKABLE QString getSelectedLocation() const;
    Q_INVOKABLE QString getSelectedDescription() const;

signals:
    void careerChanged();
    void selectedChanged(int);

private:
    QVector<WorkExperience> _items;
    float _earliest_year;
    float _latest_year;
    int _selected;

};
