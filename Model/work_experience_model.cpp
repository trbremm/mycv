#include "work_experience_model.h"

#include<QDebug>

#include "work_experience_list.h"

WorkExperienceModel::WorkExperienceModel(QObject *parent) : QAbstractListModel(parent) {
    _role_names[PriorityRole] = "priority";
    _role_names[DurationRole] = "duration";
    _role_names[StartingYearRole] = "starting_year";
    _role_names[FinalYearRole] = "final_year";
}

int WorkExperienceModel::rowCount(QModelIndex const& parent) const {
    if (parent.isValid() || !_list)
        return 0;

    return _list->items().size();
}

QVariant WorkExperienceModel::data(QModelIndex const& index, int role) const {
    if (!index.isValid() || !_list)
        return QVariant();

    WorkExperience item = _list->items().at(index.row());
    switch (role) {
        case PriorityRole:
            return QVariant(item.priority);
        case DurationRole:
            return QVariant(item.DurationInYears());
        case StartingYearRole:
            return QVariant(item.StartingYear());
        case FinalYearRole:
            return QVariant(item.FinalYear());
    }

    return QVariant();
}

QHash<int, QByteArray> WorkExperienceModel::roleNames() const {
    return _role_names;
}

WorkExperienceList *WorkExperienceModel::list() {
    return _list;
}

void WorkExperienceModel::setList(WorkExperienceList *newList) {
    _list = newList;
}
