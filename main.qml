import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.0
import "Pages"
import "Controls"

Window {
    id: window
    minimumWidth: 640
    minimumHeight: 480
    visible: true
    title: qsTr("Curriculum Vitae")

    ToolBar {

        id: header
        anchors.left: parent.left
        anchors.top: parent.top
        width: parent.width
        height: 60
        ButtonGroup {
            buttons: buttons_layout.children
        }
        Rectangle {
           id: bar_background
           anchors.fill: parent
           color: "#7e8db1"

            RowLayout {
                id: buttons_layout
                anchors.fill: parent

                HeaderButton {
                    id: info_button
                    height: header.height
                    onPressed: content_loader.sourceComponent = info_page
                    icon.name: "info"
                    icon.source: "qrc:/Icons/info.svg"
                    icon.width: 122 / 1.75
                    icon.height: 80 / 1.75
                    checked: true
                    tip_text: "Personal Information"
                }

                HeaderButton {
                    id: experiences_button
                    onPressed: content_loader.sourceComponent = work_experience_page
                    icon.source: "qrc:/Icons/job_experiences.svg"
                    icon.name: "job experiences"
                    icon.width: 103 / 2.5
                    icon.height: 123 / 2.5
                    tip_text: "Work Experience"
                }

                HeaderButton {
                    id: education_button
                    onPressed: content_loader.sourceComponent = education_page
                    icon.source: "qrc:/Icons/education.svg"
                    icon.width: 123 / 2
                    icon.height: 73 / 2
                    tip_text: "Education"
                }

                HeaderButton {
                    id: languages_button
                    onPressed: content_loader.sourceComponent = languages_page
                    icon.source: "qrc:/Icons/languages.svg"
                    icon.name: "languages"
                    icon.width: 122 / 2.5
                    icon.height: 102 / 2.5
                    tip_text: "Languages"
                }

                HeaderButton {
                    id: hobbies_button
                    onPressed: content_loader.sourceComponent = hobbies_page
                    icon.source: "qrc:/Icons/hobbies.svg"
                    icon.name: "hobbies"
                    icon.width: 123 / 2.5
                    icon.height: 110 / 2.5
                    tip_text: "Hobbies"
                }
            }
        }
    }

    Rectangle{
        id: display_area
        y: header.height
        height: window.height - header.height
        width:  window.width

        Loader {
            id: content_loader
            anchors.fill: parent
            sourceComponent: info_page
        }
        Component {
            id: info_page
            InfoPage{
            }
        }
        Component {
            id: education_page
            EducationPage {
            }
        }
        Component {
            id: work_experience_page
            WorkExperiencePage {
            }
        }
        Component {
            id: languages_page
            LanguagesPage {
            }
        }
        Component {
            id: hobbies_page
            HobbiesPage {
            }
        }
    }
}
